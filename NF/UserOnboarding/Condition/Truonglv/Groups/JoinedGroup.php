<?php

//  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
// ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
// ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
// ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
//  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
//  https://fortreeforums.xyz

namespace apathy\OnboardingTasks\Condition\Truonglv\Groups;

use NF\UserOnboarding\Condition\AbstractCondition;
use NF\UserOnboarding\Entity\Task;
use Truonglv\Entity\Group;
use XF\Job\AbstractJob;
use XF\Job\JobResult;
use XF\Mvc\Entity\Entity;

class JoinedGroup extends AbstractCondition
{
    public function getShortName()
    {
        return 'apathy\OnboardingTasks:Truonglv\Groups\JoinedGroup';
    }

    public function getOptionLabel()
    {
        return \XF::phrase('ap_ot_user_has_joined_social_group');
    }

    public function getOptions()
    {
        return [
            'social_group_id' => 'uint'
        ];
    }

    public function getConfigTemplateName()
    {
        return 'ap_ot_condition_config_social_group_specific';
    }

    public function getEventListeners()
    {
        return [
            [
                'entity_post_save',
                function (Entity $entity) {
                    if(!\XF::visitor()->user_id
                    || \XF::visitor()->user_id != $entity->member_id)
                    {
                        return;
                    }

                    $this->matchConditions(['social_group_id' => $entity->group_id]);
                },
                'Truonglv\Entity\Group'
            ]
        ];
    }

    public function isEnabled()
    {
        return isset(\XF::app()->container('addon.cache')['Truonglv/Groups']);
    }

    public function getDisplayOrder()
    {
        return 1124;
    }

    public function processExistingMatches(AbstractJob $job, Task $task, array $handlerData,
    &$position, $maxRunTime)
    {
        $userIds = $this->app->db()->fetchAllColumn("
            SELECT DISTINCT member_id
            FROM xf_tl_group_member
            WHERE group_id = ?
            AND member_id > ?
            ORDER BY member_id
            LIMIT 500",
            [$handlerData['options']['social_group_id'], $position]);

        if(!$this->markTaskAsCompletedForUserIds($task->onboarding_task_id, $userIds, $position))
        {
            return $job->complete();
        }

        return $job->resume();
    }
}
