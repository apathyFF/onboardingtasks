<?php

//  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
// ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
// ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
// ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
//  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
//  https://fortreeforums.xyz

namespace apathy\OnboardingTasks\Condition;

use NF\UserOnboarding\Condition\AbstractCondition;
use NF\UserOnboarding\Entity\Task;
use XF\Entity\UserProfile;
use XF\Job\AbstractJob;
use XF\Job\JobResult;
use XF\Mvc\Entity\Entity;

class AboutYou extends AbstractCondition
{
    public function getShortName()
    {
        return 'apathy\OnboardingTasks:AboutYou';
    }

    public function getOptionLabel()
    {
        return \XF::phrase('ap_ot_user_has_set_about_you');
    }

    public function getEventListeners()
    {
        return [
            [
                'entity_post_save',
                function (Entity $entity) {
                    if(!\XF::visitor()->user_id
                    || \XF::visitor()->user_id != $entity->user_id)
                    {
                        return;
                    }

                    if($entity->isChanged('about')
                    && $entity->about)
                    {
                        $this->matchConditions([]);
                    }
                },
                'XF\Entity\UserProfile'
            ]
        ];
    }

    public function getDisplayOrder()
    {
        return 1116;
    }

    public function processExistingMatches(AbstractJob $job, Task $task, array $handlerData,
    &$position, $maxRunTime)
    {
        $userIds = $this->app->db()->fetchAllColumn("
            SELECT DISTINCT user_id
            FROM xf_user_profile
            WHERE about <> ''
            AND user_id > ?
            ORDER BY user_id
            LIMIT 500", [$position]);

        if(!$this->markTaskAsCompletedForUserIds($task->onboarding_task_id, $userIds, $position))
        {
            return $job->complete();
        }

        return $job->resume();
    }
}
