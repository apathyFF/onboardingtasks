<?php

//  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
// ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
// ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
// ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
//  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
//  https://fortreeforums.xyz

namespace apathy\OnboardingTasks\Condition;

use NF\UserOnboarding\Condition\AbstractCondition;
use NF\UserOnboarding\Entity\Task;
use XF;
use XF\Job\AbstractJob;
use XF\Job\JobResult;
use XF\Mvc\Entity\Entity;

class MessageCount extends AbstractCondition
{
    public function getShortName()
    {
        return 'apathy\OnboardingTasks:MessageCount';
    }

    public function getOptionLabel()
    {
        return XF::phrase('ap_ot_users_message_count_is_greater_than_0');
    }

    public function getEventListeners()
    {
        return [
            [
                'entity_post_save',
                function (Entity $entity) {
                    if(!XF::visitor()->user_id
                    || $entity->isUpdate()
                    || $entity->user_id != XF::visitor()->user_id)
                    {
                        return;
                    }

                    if($entity->User->message_count > 0)
                    {
                        $this->matchConditions([]);
                    }
                },
                'XF\Entity\Post'
            ]
        ];
    }

    public function getDisplayOrder()
    {
        return 1125;
    }

    public function processExistingMatches(AbstractJob $job, Task $task, array $handlerData,
    &$position, $maxRunTime)
    {
        $userIds = $this->app->db()->fetchAllColumn(
            "SELECT DISTINCT user_id
            FROM xf_user
            WHERE message_count > ?
            AND user_id > ?
            ORDER BY user_id
            LIMIT 500", [0, $position]
        );

        if(!$this->markTaskAsCompletedForUserIds($task->onboarding_task_id, $userIds, $position))
        {
            return $job->complete();
        }

        return $job->resume();
    }
}
