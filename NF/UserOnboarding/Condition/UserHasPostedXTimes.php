<?php

//  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
// ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
// ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
// ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
//  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
//  https://fortreeforums.xyz

namespace apathy\OnboardingTasks\Condition;

use NF\UserOnboarding\Condition\AbstractCondition;
use NF\UserOnboarding\Entity\Task;
use XF;
use XF\Job\AbstractJob;
use XF\Job\JobResult;
use XF\Mvc\Entity\Entity;

class UserHasPostedXTimes extends AbstractCondition
{
    public function getShortName()
    {
        return 'apathy\OnboardingTasks:UserHasPostedXTimes';
    }

    public function getOptionLabel()
    {
        return XF::phrase('ap_ot_user_has_posted_x_times');
    }

    public function getOptions()
    {
        return [
            'num_of_posts' => 'uint'
        ];
    }

    public function getConfigTemplateName()
    {
        return 'ap_ot_condition_config_posted_x_times';
    }

    public function getEventListeners()
    {
        return [
            [
                'entity_post_save',
                function (Entity $entity) {
                    if(!XF::visitor()->user_id
                    || $entity->isUpdate()
                    || $entity->user_id != XF::visitor()->user_id)
                    {
                        return;
                    }

                    $this->matchConditions(['num_of_posts' => $entity->User->message_count]);
                },
                'XF\Entity\Post'
            ]
        ];
    }

    public function getDisplayOrder()
    {
        return 1129;
    }

    public function processExistingMatches(AbstractJob $job, Task $task, array $handlerData,
    &$position, $maxRunTime)
    {
        $userIds = $this->app->db()->fetchAllColumn(
            "SELECT DISTINCT user_id
            FROM xf_user
            WHERE message_count >= ?
            AND user_id > ?
            ORDER BY user_id
            LIMIT 500",
            [$handlerData['options']['num_of_posts'], $position]
        );

        if(!$this->markTaskAsCompletedForUserIds($task->onboarding_task_id, $userIds, $position))
        {
            return $job->complete();
        }

        return $job->resume();
    }
}
