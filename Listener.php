<?php

namespace apathy\OnboardingTasks;

use XF\Container;

class Listener
{
    public static function userOnboardingTasks(Container $container, array &$handlerClasses)
    {
        $container['ap.OtOnboardingTasks'] = function (Container $c)
        {
            return [
                'apathy\OnboardingTasks:AboutYou',
                'apathy\OnboardingTasks:Location',
                'apathy\OnboardingTasks:Following',
                'apathy\OnboardingTasks:ReactedToPost',
                'apathy\OnboardingTasks:ReactedToSpecificPost',
                'apathy\OnboardingTasks:PrimaryGroup',
                'apathy\OnboardingTasks:SecondaryGroup',
                'apathy\OnboardingTasks:SpecificTheme',
                'apathy\OnboardingTasks:Truonglv\Groups\JoinedGroup',
                'apathy\OnboardingTasks:MessageCount',
                'apathy\OnboardingTasks:LicenseVerification',
                'apathy\OnboardingTasks:PostedInCategory',
                'apathy\OnboardingTasks:UserHasPostedXTimes'
            ];
        };

        if(isset($container['ap.OtOnboardingTasks']) && isset($handlerClasses))
        {
            array_push($handlerClasses, ...$container['ap.OtOnboardingTasks']);
        }
    }
}
